package com.example.demo.Repository;

import com.example.demo.Model.Person;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void run() {
        // Insert
        var p = new Person(1, "Alice");
        entityManager.persist(p);
        p.setName("ALICE");

        // Select
        // var p = entityManager.find(Person.class, 1);
        // p.setName("ALICE");

        // Detached
        // var p = entityManager.find(Person.class, 1);
        // entityManager.detach(p);
        // p.setName("Wonderland");
    }
}
