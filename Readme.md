# Java Spring Data JPA & Entity Manager
A simple repository to demonstrate the Entity Manager lifecycle.

# Getting Started
- Clone this repo
- Create an `application.properties` file based from this [template](./src/main/resources/application.properties.template) file
- Observe the lifecycle in the `run()` method for the [PersonRepository](./src/main/java/com/example/demo/Repository/PersonRepository.java) class

# Videos
- [JPA & Hibernate Lifecycle (7m)](https://youtu.be/Y7PpjerZkc0)

# Notes
- Built with IntelliJ 2022.2 and JDK 19
